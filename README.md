# Simple event log for web-service using mongodb

## Build and run
```console
go build -o server .
./server
```

## Requests examples to work with running server
```console
// Posting new Impression event:
curl -H "Content-Type: application/json" -d '{"data_time":"12.06.2018-20:22:03", "transaction_id":"1", "ad_type":"craziness", "user_id":"2"}' http://localhost:8080/events/impression

// Show all posting impressions for user id [USER_ID]:
 curl -H "Content-Type: application/json"  http://localhost:8080/events/impression/[USER_ID]

// Posting new Click event:
curl -H "Content-Type: application/json" -d '{"data_time":"12.06.2018-20:22:03", "transaction_id":"1", "ad_type":"cool things", "time_to_click":"8", "user_id":"2"}' http://localhost:8080/events/click

// Show all posting Clicks events for user id [USER_ID]:
curl -H "Content-Type: application/json"  http://localhost:8080/events/click/[USER_ID]

// Posting new completion event:
curl -H "Content-Type: application/json" -d '{"data_time":"12.06.2018-20:22:03", "transaction_id":"1", "user_id":"2"}' http://localhost:8080/events/completion

// Show all completion events for user id [USER_ID]:
curl -H "Content-Type: application/json"  http://localhost:8080/events/completion/[USER_ID]
```

# Tests
```console
$ go test
2018/01/08 18:02:10 POST		impression	263.108µs
2018/01/08 18:02:10 GET		impressionShow	218.963µs
2018/01/08 18:02:10 POST		click	185.578µs
2018/01/08 18:02:10 GET		clickShow	167.564µs
2018/01/08 18:02:10 POST		completion	197.445µs
2018/01/08 18:02:10 GET		completionShow	162.266µs
PASS
ok  	github.com/bronhaim/go-http-event-log	0.007s

- for benchmark testing - "go test -bench=."
```
