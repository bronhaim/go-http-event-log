package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	mgo "gopkg.in/mgo.v2"
)

func initLocalDbConnection() {
	// start mongo session - this requires to run mongod on localhost
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	mgoSession = session
}

func TestPostPostAndGetNewImpression(t *testing.T) {
	initLocalDbConnection()
	defer mgoSession.Close()
	impression := Impression{
		DataTime:      "1.1.1",
		TransactionID: "1",
		AdType:        "cool",
		UserID:        "999",
	}
	//using our router to server the request
	router := NewRouter()

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(impression)

	reqPost, err := http.NewRequest("POST", "/events/impression", b)
	if err != nil {
		t.Fatal(err)
	}

	rrPost := httptest.NewRecorder()
	router.ServeHTTP(rrPost, reqPost)

	if status := rrPost.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	var impressionRet Impression
	err = json.NewDecoder(rrPost.Body).Decode(&impressionRet)

	if !reflect.DeepEqual(impression, impressionRet) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			impression, impressionRet)
	}

	reqGet, err := http.NewRequest("GET", "/events/impression/999", nil)
	if err != nil {
		t.Fatal(err)
	}
	rrGet := httptest.NewRecorder()
	router.ServeHTTP(rrGet, reqGet)
	if status := rrGet.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	err = json.NewDecoder(rrGet.Body).Decode(&impressionRet)
	if !reflect.DeepEqual(impression, impressionRet) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			impression, impressionRet)
	}
}

func TestPostPostAndGetNewClick(t *testing.T) {
	initLocalDbConnection()
	defer mgoSession.Close()
	click := Click{
		DataTime:      "1.1.1",
		TransactionID: "1",
		AdType:        "cool",
		UserID:        "999",
		TimeToClick:   "12:00",
	}
	//using our router to server the request
	router := NewRouter()

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(click)

	reqPost, err := http.NewRequest("POST", "/events/click", b)
	if err != nil {
		t.Fatal(err)
	}

	rrPost := httptest.NewRecorder()
	router.ServeHTTP(rrPost, reqPost)

	if status := rrPost.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	var clickRet Click
	err = json.NewDecoder(rrPost.Body).Decode(&clickRet)

	if !reflect.DeepEqual(click, clickRet) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			click, clickRet)
	}

	reqGet, err := http.NewRequest("GET", "/events/click/999", nil)
	if err != nil {
		t.Fatal(err)
	}
	rrGet := httptest.NewRecorder()
	router.ServeHTTP(rrGet, reqGet)
	if status := rrGet.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	err = json.NewDecoder(rrGet.Body).Decode(&clickRet)
	if !reflect.DeepEqual(click, clickRet) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			click, clickRet)
	}
}

func TestPostPostAndGetNewCompletion(t *testing.T) {
	initLocalDbConnection()
	defer mgoSession.Close()
	completion := Completion{
		DataTime:      "1.1.1",
		TransactionID: "1",
		UserID:        "999",
	}
	//using our router to server the request
	router := NewRouter()

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(completion)

	reqPost, err := http.NewRequest("POST", "/events/completion", b)
	if err != nil {
		t.Fatal(err)
	}

	rrPost := httptest.NewRecorder()
	router.ServeHTTP(rrPost, reqPost)

	if status := rrPost.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	var completionRet Completion
	err = json.NewDecoder(rrPost.Body).Decode(&completionRet)

	if !reflect.DeepEqual(completion, completionRet) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			completion, completionRet)
	}

	reqGet, err := http.NewRequest("GET", "/events/completion/999", nil)
	if err != nil {
		t.Fatal(err)
	}
	rrGet := httptest.NewRecorder()
	router.ServeHTTP(rrGet, reqGet)
	if status := rrGet.Code; status != http.StatusFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	err = json.NewDecoder(rrGet.Body).Decode(&completionRet)
	if !reflect.DeepEqual(completion, completionRet) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			completion, completionRet)
	}
}

func BenchmarkPostClickRequests(b *testing.B) {
	initLocalDbConnection()
	defer mgoSession.Close()
	click := Click{
		DataTime:      "1.1.1",
		TransactionID: "1",
		AdType:        "cool",
		UserID:        "999",
		TimeToClick:   "12:00",
	}
	//using our router to server the request
	router := NewRouter()

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(click)

	reqPost, err := http.NewRequest("POST", "/events/click", buf)
	if err != nil {
		b.Fatal(err)
	}

	rrPost := httptest.NewRecorder()
	var clickRet Click

	// post requests function b.N times
	for n := 0; n < b.N; n++ {
		router.ServeHTTP(rrPost, reqPost)

		if status := rrPost.Code; status != http.StatusCreated {
			b.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		err = json.NewDecoder(rrPost.Body).Decode(&clickRet)

		if !reflect.DeepEqual(click, clickRet) {
			b.Errorf("handler returned unexpected body: got %v want %v",
				click, clickRet)
		}
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkPostCompletionRequests(b *testing.B) {
	initLocalDbConnection()
	defer mgoSession.Close()
	completion := Completion{
		DataTime:      "1.1.1",
		TransactionID: "1",
		UserID:        "999",
	}
	//using our router to server the request
	router := NewRouter()

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(completion)

	reqPost, err := http.NewRequest("POST", "/events/completion", buf)
	if err != nil {
		b.Fatal(err)
	}

	rrPost := httptest.NewRecorder()
	var completionRet Completion

	// post requests function b.N times
	for n := 0; n < b.N; n++ {
		router.ServeHTTP(rrPost, reqPost)

		if status := rrPost.Code; status != http.StatusCreated {
			b.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		err = json.NewDecoder(rrPost.Body).Decode(&completionRet)

		if !reflect.DeepEqual(completion, completionRet) {
			b.Errorf("handler returned unexpected body: got %v want %v",
				completion, completionRet)
		}
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkPostImpressionRequests(b *testing.B) {
	initLocalDbConnection()
	defer mgoSession.Close()
	impression := Impression{
		DataTime:      "1.1.1",
		TransactionID: "1",
		AdType:        "cool",
		UserID:        "999",
	}
	//using our router to server the request
	router := NewRouter()

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(impression)

	reqPost, err := http.NewRequest("POST", "/events/impression", buf)
	if err != nil {
		b.Fatal(err)
	}

	rrPost := httptest.NewRecorder()
	var impressionRet Impression

	// post requests function b.N times
	for n := 0; n < b.N; n++ {
		router.ServeHTTP(rrPost, reqPost)

		if status := rrPost.Code; status != http.StatusCreated {
			b.Errorf("handler returned wrong status code: got %v want %v",
				status, http.StatusOK)
		}

		err = json.NewDecoder(rrPost.Body).Decode(&impressionRet)

		if !reflect.DeepEqual(impression, impressionRet) {
			b.Errorf("handler returned unexpected body: got %v want %v",
				impression, impressionRet)
		}
		if err != nil {
			b.Fatal(err)
		}
	}
}
