package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Impression is type that represents video that was displayed
type Impression struct {
	DataTime      string `json:"data_time"`
	TransactionID string `json:"transaction_id"`
	AdType        string `json:"ad_type"`
	UserID        string `json:"user_id"`
}

// Click is type that represents video that was clicked
type Click struct {
	DataTime      string `json:"data_time"`
	TransactionID string `json:"transaction_id"`
	AdType        string `json:"ad_type"`
	UserID        string `json:"user_id"`
	TimeToClick   string `json:"time_to_click"`
}

// Completion is type that represents fields for user that finished watching a movie
type Completion struct {
	DataTime      string `json:"data_time"`
	TransactionID string `json:"transaction_id"`
	// NOTE: added userid to allow the GET api
	UserID string `json:"user_id"`
}

var mgoSession *mgo.Session

// TODO: move to conf file
const dbName = "eventdb"
const contentType = "application/json; charset=UTF-8"
const port = ":8080"
const dbAddress = "localhost"

func impressionEventPOST(w http.ResponseWriter, r *http.Request) {
	var impression Impression
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 5000))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &impression); err != nil {
		errorRes(w, err)
		return
	}

	c := mgoSession.DB(dbName).C("impression")
	if err := c.Insert(impression); err != nil {
		errorRes(w, err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(impression); err != nil {
		panic(err)
	}
}

func clickEventPOST(w http.ResponseWriter, r *http.Request) {
	var click Click
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 5000))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &click); err != nil {
		errorRes(w, err)
		return
	}

	c := mgoSession.DB(dbName).C("click")
	if err := c.Insert(click); err != nil {
		errorRes(w, err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(click); err != nil {
		panic(err)
	}
}

func completionEventPOST(w http.ResponseWriter, r *http.Request) {
	var completion Completion
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 5000))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	if err := json.Unmarshal(body, &completion); err != nil {
		errorRes(w, err)
		return
	}

	c := mgoSession.DB(dbName).C("completion")
	if err := c.Insert(completion); err != nil {
		errorRes(w, err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(completion); err != nil {
		panic(err)
	}
}

func impressionEventGET(w http.ResponseWriter, r *http.Request) {
	result := Impression{}
	vars := mux.Vars(r)
	userID := vars["user_id"]

	c := mgoSession.DB(dbName).C("impression")
	if err := c.Find(bson.M{"userid": userID}).One(&result); err != nil {
		errorRes(w, err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusFound)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		panic(err)
	}
}

func clickEventGET(w http.ResponseWriter, r *http.Request) {
	result := Click{}
	vars := mux.Vars(r)
	userID := vars["user_id"]

	c := mgoSession.DB(dbName).C("click")
	if err := c.Find(bson.M{"userid": userID}).One(&result); err != nil {
		errorRes(w, err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusFound)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		panic(err)
	}
}

// completionEventGET fetching from db completion record for requested user id
func completionEventGET(w http.ResponseWriter, r *http.Request) {
	result := Completion{}
	vars := mux.Vars(r)
	userID := vars["user_id"]

	c := mgoSession.DB(dbName).C("completion")
	if err := c.Find(bson.M{"userid": userID}).One(&result); err != nil {
		errorRes(w, err)
		return
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(http.StatusFound)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		panic(err)
	}
}

// errorRes reused func for sending 422 response
func errorRes(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(422)
	if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)
	}
}

func main() {
	session, err := mgo.Dial(dbAddress)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	mgoSession = session

	log.Fatal(http.ListenAndServe(port, NewRouter()))
}
