package main

import "net/http"

// Route describes route in routes list
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes list of route which is used by the NewRouter()
type Routes []Route

var routes = Routes{
	Route{
		"impression",
		"POST",
		"/events/impression",
		impressionEventPOST,
	},
	Route{
		"impressionShow",
		"GET",
		"/events/impression/{user_id}",
		impressionEventGET,
	},
	Route{
		"click",
		"POST",
		"/events/click",
		clickEventPOST,
	},
	Route{
		"clickShow",
		"GET",
		"/events/click/{user_id}",
		clickEventGET,
	},
	Route{
		"completion",
		"POST",
		"/events/completion",
		completionEventPOST,
	},
	Route{
		"completionShow",
		"GET",
		"/events/completion/{user_id}",
		completionEventGET,
	},
}
